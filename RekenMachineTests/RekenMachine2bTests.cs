﻿using System;
using Xunit;
using WindesheimAD2021;

namespace RekenMachineTests
{
    public class RekenMachine2bTests
    {
        RekenMachine2 Apparaat = new RekenMachine2();

        [Theory]
        [InlineData(4, 3, 7)]
        [InlineData(21, 5.25, 26.25)]
        [InlineData(double.MaxValue, 5, double.MaxValue)]
        public void Plus_SimpeleOptellingen(double x, double y, double expected)
        {
            // Arrange

            // Act
            double actual = Apparaat.Plus(x, y);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(8, 4, 2)]
        public void Deel_SimpeleDeling(double x, double y, double expected)
        {
            // Arrange

            // Act
            double actual = Apparaat.Deel(x, y);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Deel_DeelDoorNul()
        {
            // Arrange
            double expected = 0;

            // Act
            double actual = Apparaat.Deel(15, 0);

            // Assert
            Assert.Equal(expected, actual);
        }
    }
}