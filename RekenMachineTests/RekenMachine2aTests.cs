﻿using System;
using Xunit;
using WindesheimAD2021;

namespace RekenMachineTests
{
    public class RekenMachine2aTests
    {
        RekenMachine2 Apparaat = new RekenMachine2();

        [Fact]
        public void Plus_SimpleTest()
        {
            // Arrange
            double expected = 13;

            // Act
            double actual = Apparaat.Plus(10, 3);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Min_SimpleTest()
        {
            // Arrange
            double expected = 13;

            // Act
            double actual = Apparaat.Min(16, 3);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Keer_SimpleTest()
        {
            // Arrange
            double expected = 14;

            // Act
            double actual = Apparaat.Keer(7, 2);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Deel_SimpleTest()
        {
            // Arrange
            double expected = 12;

            // Act
            double actual = Apparaat.Deel(144, 12);

            // Assert
            Assert.Equal(expected, actual);
        }
    }
}