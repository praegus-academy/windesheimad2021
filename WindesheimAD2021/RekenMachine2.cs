﻿using System;
namespace WindesheimAD2021
{
    public class RekenMachine2
    {
        public double Plus(double x, double y)
        {
            return x + y;
        }

        public double Min(double x, double y)
        {
            return x - y;
        }

        public double Keer(double x, double y)
        {
            return x * y;
        }

        public double Deel(double x, double y)
        {
            if (y != 0)
            {
                return x / y;
            }
            else
            {
                // Oplossing deel door 0
                return 0;
            }
        }
    }
}