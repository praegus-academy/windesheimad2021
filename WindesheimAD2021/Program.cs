﻿using System;

namespace WindesheimAD2021
{
    class Program
    {
        static void Main(string[] args)
        {
            int max = int.MaxValue;
            int min = int.MinValue;
            Console.WriteLine($"The range of integers is {min} to {max}");
            decimal maxDec = int.MaxValue;
            decimal minDec = int.MinValue;
            Console.WriteLine($"The range of decimals is {minDec} to {maxDec}");
            double maxD = double.MaxValue;
            double minD = double.MinValue;
            Console.WriteLine($"The range of doubles is {minD} to {maxD}");

            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine("===================================================================================");
            Console.WriteLine(" ██████╗ █████╗ ██╗      ██████╗██╗   ██╗██╗      █████╗ ████████╗ ██████╗ ██████╗");
            Console.WriteLine("██╔════╝██╔══██╗██║     ██╔════╝██║   ██║██║     ██╔══██╗╚══██╔══╝██╔═══██╗██╔══██╗");
            Console.WriteLine("██║     ███████║██║     ██║     ██║   ██║██║     ███████║   ██║   ██║   ██║██████╔╝");
            Console.WriteLine("██║     ██╔══██║██║     ██║     ██║   ██║██║     ██╔══██║   ██║   ██║   ██║██╔══██╗");
            Console.WriteLine("╚██████╗██║  ██║███████╗╚██████╗╚██████╔╝███████╗██║  ██║   ██║   ╚██████╔╝██║  ██║");
            Console.WriteLine(" ╚═════╝╚═╝  ╚═╝╚══════╝ ╚═════╝ ╚═════╝ ╚══════╝╚═╝  ╚═╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝");
            Console.WriteLine("===================================================================================");
            Console.ResetColor();

            calculatorFlow();

        }


        private static void calculatorFlow()
        {
            RekenMachine2 calculator = new RekenMachine2();

            Console.WriteLine("Pick your operation:");
            Console.WriteLine("A: Add");
            Console.WriteLine("S: Substract");
            Console.WriteLine("M: Multiply");
            Console.WriteLine("D: divide");
            Console.WriteLine("Q: quit");

            ConsoleKeyInfo operationKey = Console.ReadKey();

            if (!operationKey.KeyChar.Equals('q'))
            {
                double arg1 = 0d;
                double arg2 = 0d;

                Console.WriteLine("First number:");
                try
                {
                    arg1 = double.Parse(Console.ReadLine());
                }

                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("ERROR: " + e.Message);
                    Console.ResetColor();
                }

                Console.WriteLine("Second number:");
                try
                {
                    arg2 = double.Parse(Console.ReadLine());
                }

                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("ERROR: " + e.Message);
                    Console.ResetColor();
                }

                switch (operationKey.KeyChar)
                {
                    case 'a':
                        Console.WriteLine("Your Answer: " + calculator.Plus(arg1, arg2));
                        break;
                    case 's':
                        Console.WriteLine("Your Answer: " + calculator.Min(arg1, arg2));
                        break;
                    case 'm':
                        Console.WriteLine("Your Answer: " + calculator.Keer(arg1, arg2));
                        break;
                    case 'd':
                        Console.WriteLine("Your Answer: " + calculator.Deel(arg1, arg2));
                        break;
                    default:
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("ERROR: " + operationKey.KeyChar + " is not a valid operation!");
                        Console.ResetColor();
                        break;
                }
                calculatorFlow();
            }
        }
    }
}